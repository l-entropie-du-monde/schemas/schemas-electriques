// MATRICES de transformation

const rotationMatrix_D = [
    [0, -1],
    [1,  0]
];
const rotationMatrix_G = [
    [0, 1],
    [-1,  0]
];

const mirrorMatrix_H = [
    [-1, 0],
    [ 0, 1]
];

const mirrorMatrix_V = [
    [1, 0],
    [ 0, -1]
];

// Fonction pour multiplier deux matrices 2x2
function multiplyMatrices(a, b) {
    return [
        [
            a[0][0] * b[0][0] + a[0][1] * b[1][0],
            a[0][0] * b[0][1] + a[0][1] * b[1][1]
        ],
        [
            a[1][0] * b[0][0] + a[1][1] * b[1][0],
            a[1][0] * b[0][1] + a[1][1] * b[1][1]
        ]
    ];
}

// utilisation : 
// rotation
// composants[id].trMatrix = multiplyMatrices(composants[id].trMatrix, rotationMatrix);

// miroir
// composants[id].trMatrix = multiplyMatrices(composants[id].trMatrix, rmirorMatrix);


// gestion des composants 

async function loadAndModifySVG(url, color, trM) {
    const response = await fetch(url);
    let svgText = await response.text();
    svgText = svgText.replace(/(fill|stroke):#000000/g, `$1:${color}`);

    const svgBlob = new Blob([svgText], { type: 'image/svg+xml' });
    const objectUrl = URL.createObjectURL(svgBlob);

    const img = new Image();
    return new Promise((resolve) => {
        img.onload = function () {
            URL.revokeObjectURL(objectUrl); // Libérer l'URL une fois l'image chargée

            // Créer un canvas temporaire pour appliquer la rotation
            const tempCanvas = document.createElement('canvas');
            const tempCtx = tempCanvas.getContext('2d');

            // Récupérer les valeurs de la matrice
            const [a, c] = trM[0];
            const [b, d] = trM[1];

            // Inverser les dimensions si la matrice correspond à une rotation de 90° ou 270°
            if (a === 0 && d === 0) {
                tempCanvas.width  = img.height;
                tempCanvas.height = img.width;
            } else {
                tempCanvas.width  = img.width;
                tempCanvas.height = img.height;
            }

            // Appliquer la matrice de transformation à `tempCtx`
            tempCtx.setTransform(
                a, // a
                b, // b
                c, // c
                d, // d
                tempCanvas.width / 2, // e (translation en x pour centrer)
                tempCanvas.height / 2 // f (translation en y pour centrer)
            );

            // Dessiner l'image sur le canvas temporaire
            //tempCtx.drawImage(img, 0, 0);
            tempCtx.drawImage(img, -img.width / 2, -img.height / 2);

            // Créer une nouvelle image à partir du canvas transformé
            const trImg = new Image();
            trImg.onload = function () {
                resolve(trImg);
            };
            trImg.src = tempCanvas.toDataURL();
        };
        img.src = objectUrl;
    });
}

function supprComposant(id) {
    composants.splice(id, 1);
    // suppriemr aussi les conducteurs liés à ce composant
    selectedCompo = -1;
    drawSelectCanvas()
    drawCanvas();
}


async function rotatComposant(id) {

// échange hauteur largeur
    [composants[id].width, composants[id].height] = [composants[id].height, composants[id].width];

            // Récupérer les valeurs de la matrice
            const [a, c] = composants[id].trMatrix[0];
            const [b, d] = composants[id].trMatrix[1];

            const sig = a +2*b + 4*c + 8*d;  // la signature est 2, 6, 7, 9, -2, -6, -7 ou -9
            
    if ((Math.abs(sig) == 9)||(Math.abs(sig) == 2)) {
        composants[id].trMatrix = multiplyMatrices(composants[id].trMatrix, rotationMatrix_D);
    } else {
        composants[id].trMatrix = multiplyMatrices(composants[id].trMatrix, rotationMatrix_G);    
    };    

    selectSvgImage = await loadAndModifySVG(composants[id].image, '#00CC00', composants[id].trMatrix);

// petit clignottement : p-ê effacer la zone de fond sous le compo avant de rotationner
    drawSelectCanvas();
    drawCanvas();
}

async function mirrorComposant(id) {

            // Récupérer les valeurs de la matrice
            const [a, c] = composants[id].trMatrix[0];
            const [b, d] = composants[id].trMatrix[1];
            
            const sig = a +2*b + 4*c + 8*d; 

    if ((Math.abs(sig) == 9)||(Math.abs(sig) == 7)) {
        composants[id].trMatrix = multiplyMatrices(composants[id].trMatrix, mirrorMatrix_H);    
    } else {
        composants[id].trMatrix = multiplyMatrices(composants[id].trMatrix, mirrorMatrix_V);    
    };

    selectSvgImage = await loadAndModifySVG(composants[id].image, '#00CC00', composants[id].trMatrix);

// petit clignottement bc plus discret vu la pluspart des dessins
    drawSelectCanvas();
    drawCanvas();
}


