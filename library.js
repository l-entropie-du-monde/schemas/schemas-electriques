const data = {
    "bibliotheque": [
        {
            "point de dérivation": [
                {
                    "image": "img/deriv.svg",
                    "ID":"Pt dériv",
                    "width": 21,
                    "height": 21,
                    "points": [
                        { "name": "point", "x": 10, "y": 10 }
                    ]
                }
            ],
            "générateur": [
                {
                    "image": "img/generateur.svg",
                    "ID":"G",
                    "width": 59,
                    "height": 59,
                    "points": [
                        { "name": "A", "x": 29, "y": 1 },
                        { "name": "B", "x": 29, "y": 59 }
                    ]
                }
            ],
            "lampe": [
                {
                    "image": "img/lampe.svg",
                    "ID":"L",
                    "width": 59,
                    "height": 59,
                    "points": [
                        { "name": "A", "x": 29, "y": 1 },
                        { "name": "B", "x": 29, "y": 59 }
                    ]
                }
            ],
            "résistance": [
                {
                    "image": "img/resistance.svg",
                    "ID":"R",
                    "width": 59,
                    "height": 21,
                    "points": [
                        { "name": "A", "x": 1, "y": 10 },
                        { "name": "B", "x": 59, "y": 10 }
                    ]
                }
            ],
            "DEL": [
                {
                    "image": "img/DEL.svg",
                    "ID":"DEL",
                    "width": 59,
                    "height": 46,
                    "points": [
                        { "name": "A", "x": 1, "y": 29 },
                        { "name": "B", "x": 59, "y": 29 }
                    ]
                }
            ],
            "transistor": [
                {
                    "image": "img/transistor.svg",
                    "ID":"T",
                    "width": 54,
                    "height": 59,
                    "points": [
                        { "name": "émetteur", "x": 36, "y": 59 },
                        { "name": "base", "x": 1, "y": 29 },
                        { "name": "collecteur", "x": 36, "y": 1 }
                    ]
                }
            ]
        }
    ]
};

