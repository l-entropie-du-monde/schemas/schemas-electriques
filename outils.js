const helpButton = document.getElementById('helpButton');
const helpWindow = document.getElementById('helpWindow');
const toolWindow = document.getElementById('toolWindow');
const toolbar = document.getElementById('toolbar');
const toolHeader = document.getElementById('toolHeader');
const helpHeader = document.getElementById('helpHeader');
const helpResizer = document.getElementById('helpResizer');
//const cnv = document.getElementById('drawingCanvas');

let offsetX = 0, offsetY = 0, //
isResizingHelp = false, isDraggingHelp = false, //
isResizingTool = false, isDraggingTool = false;

// Afficher/masquer la fenêtre d'aide

btnCloseHelp.addEventListener('click', () => {
    helpWindow.style.display = 'none';
});

helpButton.addEventListener('click', () => {
    helpWindow.style.display = (helpWindow.style.display === 'block') ? 'none' : 'block';
    
    // Affichage des infos de débug dans la fenêtre
    if (helpWindow.style.display === 'block') {
        const helpText = document.getElementById('helpText');
        helpText.value = "Debug...\n";
        helpText.value += "canvas.width  = "+cnv.width+"\n";
        helpText.value += "canvas.height = "+cnv.height+"\n";
        
        let strcomposants = '';

        // Parcourir le tableau et formater les objets dans une chaîne
        composants.forEach(item => {
            strcomposants += `TYPE: ${item.type}, ID: ${item.ID}, X: ${item.x},  Y: ${item.y}\n`;
        });

        // Ajouter le contenu au textarea
        helpText.value += strcomposants;
        
        helpText.value += "selectionné : " + String(selectedCompo) + "  \n";
        
                    // Récupérer les valeurs de la matrice
            const [a, c] = composants[selectedCompo].trMatrix[0];
            const [b, d] = composants[selectedCompo].trMatrix[1];
            
         helpText.value += "a = "+a+" c = "+c+"\n";
         helpText.value += "b = "+b+" d = "+d+"\n";
        
       // strcomposants =  JSON.stringify(composants[selectedCompo].trMatrix);
       // helpText.value += strcomposants
        }
 });

// bouton rafraichir
drawButton.addEventListener('click', () => {
    drawCanvas();
    drawSelectCanvas();
});

// Rendre les fenètres déplaçables

helpHeader.addEventListener('mousedown', (e) => {
    isDraggingHelp = true;
    isDraggingTool = false;
    isResizingHelp = false;
    isResizingTool = false;
    
    offsetX = e.clientX - helpWindow.offsetLeft;
    offsetY = e.clientY - helpWindow.offsetTop;
});

toolHeader.addEventListener('mousedown', (e) => {
    isDraggingTool = true;
    isDraggingHelp = false;
    isResizingHelp = false;
    isResizingTool = false;

    offsetX = e.clientX - toolWindow.offsetLeft;
    offsetY = e.clientY - toolWindow.offsetTop;
});

// Rendre les fenêtres redimentionnables
helpResizer.addEventListener('mousedown', (e) => {
    isDraggingHelp = false;
    isDraggingTool = false;
    isResizingHelp = true;
    isResizingTool = false;
    
    offsetX = e.clientX - helpWindow.offsetLeft;
    offsetY = e.clientY - helpWindow.offsetTop;
});

toolResizer.addEventListener('mousedown', (e) => {
    isDraggingHelp = false;
    isDraggingTool = false;
    isResizingHelp = false;
    isResizingTool = true;
    
    offsetX = e.clientX - toolWindow.offsetLeft;
    offsetY = e.clientY - toolWindow.offsetTop;
});


// ----------------------------------------------------------------------------
// Script pour redimensionner les fenêtres


document.addEventListener('mousemove', (e) => {
    if (isDraggingTool) {
        toolWindow.style.left = `${e.clientX - offsetX}px`;
        toolWindow.style.top = `${e.clientY - offsetY}px`;
    }
    if (isDraggingHelp) {
        helpWindow.style.left = `${e.clientX - offsetX}px`;
        helpWindow.style.top = `${e.clientY - offsetY}px`;
    }
    if (isResizingHelp) {
        const newWidth = e.clientX - helpWindow.getBoundingClientRect().left;
        const newHeight = e.clientY - helpWindow.getBoundingClientRect().top;
        helpWindow.style.width = `${newWidth}px`;
        helpWindow.style.height = `${newHeight}px`;
    }
    if (isResizingTool) {
        const newHeight = e.clientY - toolWindow.getBoundingClientRect().top;
        
        if (newHeight < 300) {
            toolWindow.style.height ='300px'; // taille minimum
        } else {
            if (e.clientY < cnv.height - 10) {
                toolWindow.style.height = `${newHeight}px`;
                toolbar.style.height = `${newHeight - 45}px`; // valeur codée en dur, c'est moche ;-)
            } else {
                console.log("ça dépend ça dépasse");
            }
        }
        
        // si toolWindow est plus petit que toolbar saymoche
        // si c'est plus bas que canvas aussi
   //     console.log("e.clientY = ", e.clientY);
   //     console.log("toolWindow.style.height = ", toolWindow.style.height);
   //     let toolWindowHeight = parseInt(window.getComputedStyle(toolWindow).height, 10);
//        let newToolbarHeight = toolWindowHeight - 45;  
  //      toolbar.style.height = newToolbarHeight + 'px';
    }
});

document.addEventListener('mouseup', () => {
    isDraggingHelp = false;
    isDraggingTool = false;
    isResizingHelp = false;
    isResizingTool = false;
});


