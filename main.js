// Variables globales pour stocker la position de départ et de fin
let dragStartX, dragStartY, dragEndX, dragEndY;
let compoEvent = -1; // le composant concerné par les évemenemnts souris

// structure du schéma --------------------------------------------------------
let composants = []; // tableau pour stocker les composants

// les composants sont de la forme : 
//    const newObject = {
//        type:     ,       // par ex. lampe transistor
//        ID                // T sera complété par le numéro
//        nom       ,       // T42
//        image:    ,       // le dessin issu du fichier
//        x:        ,       // position du coin haut-gauche
//        y:        ,
//        width:    ,       // taille    
//        height:   ,
//        trMatrix          // matrice de transformation
//        rotation:  0, 1 2 ou 3 compté en 1/4 de tour horaire
//        miroirGD : false/true // miroir gauche droite
//        bornes:   ,       // Ajouter la liste complète des bornes
//    };

// composant selectionné : -1 si aucun, l'index du tableau sinon
let selectedCompo = -1;

// Structure globale pour stocker les boutons pour le compo sélectionné
let editBtn = [
    { name: 'suppr', x: 0, y: 0, width: 15, height: 15, active: false },
    { name: 'rotat', x: 0, y: 0, width: 15, height: 15, active: false },
    { name: 'mirGD', x: 0, y: 0, width: 15, height: 15, active: false }
];

let tmpSvgImage = null;  // Variable globale pour stocker l'image modifiée
let selectSvgImage = null;  // Variable globale pour stocker l'image modifiée séléctionnée


// Canvas et contextes
const cnvparent = document.getElementById('gribouille');

// pour les dessins temporaires
const cnv = document.getElementById('drawingCanvas');
const ctxDraw = cnv.getContext('2d');

// pour les dessins persistants
const fond = document.getElementById('fond');
const ctxFond = fond.getContext('2d');


// On charge la bibliothèque au démarrage -------------------------------------

// Fonction pour ajuster la taille du canvas à celle du conteneur
function resizeCanvas() {
    cnv.width  = cnvparent.clientWidth;   // Largeur du div parent
    cnv.height = cnvparent.clientHeight;  // Hauteur du div parent
    fond.width  = cnv.width;
    fond.height = cnv.height;
    // ajouter le dessin complet du canvas
    drawCanvas();
}

window.addEventListener('resize', resizeCanvas);

window.onload = function() {
    const toolbar = document.getElementById('toolbar');
    const bibliotheque = data.bibliotheque[0];

    // Parcourir les objets dans "bibliotheque"
    Object.keys(bibliotheque).forEach(key => {
        bibliotheque[key].forEach(item => {
            const img = document.createElement('img');
            img.src = item.image;
            img.className = 'compoBib'; // utilisé pour le startDrag
            img.width = item.width;
            img.height = item.height;
            img.alt = key;  // Utiliser le nom de la clé comme alt
            toolbar.appendChild(img);  // Ajouter l'image à la toolbar
        });
    });
    
    resizeCanvas();
    
    // dragstart pour les composants de la bib
    document.querySelectorAll('.compoBib').forEach(compoBib => {
         compoBib.addEventListener('dragstart', function(e) {
            // données à transférer
            const rect = e.target.getBoundingClientRect();
            const data = {
                src: e.target.src,                      // Lien source de l'image
                type: e.target.alt,                     // le alt est le type "générateur", "lampe" etc.
                startX: Math.trunc(e.clientX - rect.left),  // Position relatif X du clic
                startY: Math.trunc(e.clientY - rect.top),    // Position Y du clic
                height: e.target.height,                    // taille
                width: e.target.width,
                rotation: 0,                                // orientation initiale
                miroirGD: false                             // pas retourné
            };
            
            // json pour envoyer tout ça
            e.dataTransfer.setData('application/json', JSON.stringify(data));
        });
    });

};




// Drag drop de construction --------------------------------------------------

// Fonction pour générer un ID unique
function uniqueID(baseID) {
    let fooCount = composants.filter(item => item.ID.startsWith(baseID)).length;

    if (fooCount === 0) {
        // S'il n'y a pas encore de "foo", retourner juste "foo"
        return baseID;
    } else {
        // Si "foo" existe déjà, renommer le premier en "foo1" et générer "foo2"
        let firstFoo = composants.find(item => item.ID === baseID);
        if (firstFoo) {
            firstFoo.ID = baseID + '1';  // Renomme le premier "foo" en "foo1"
        }
        return baseID + (fooCount + 1);  // Crée "foo2", "foo3", etc.
    }
}


// DROP de construction sur le canvas
cnv.addEventListener('drop', function(e) {
    e.preventDefault();
    
    // désérialiser les données
    const data = JSON.parse(e.dataTransfer.getData('application/json'));
    const img = new Image();
    img.src = data.src;
    
    const rect = cnv.getBoundingClientRect();
    const x = e.clientX - rect.left - data.startX;
    const y = e.clientY - rect.top  - data.startY;
        
    img.onload = function() {
//        // Dessiner l'image sur le canvas à la position du drop
        ctxFond.drawImage(img, x, y); // Ajuster la taille si nécessaire
    };
    
    // Créer un nouvel objet avec les informations sur le compo et l'ajouter au tableau
    // ajouter nom à partir de type : lampe1 DEL3 etc.
    let result;
    switch (data.type) {
      case 'générateur':
        result = 'G'; break;
      case 'résistance':
        result = 'R'; break;
      case 'transistor':
        result = 'T'; break;
      case 'lampe':
        result = 'L'; break;
      case 'DEL':
        result = 'DEL'; break;
      case 'point de dérivation':
        result = 'pt'; break;
      default:
        result = 'X';
    }
// numéroter en fonction de l'existant
    result = uniqueID(result);
    
    const newObject = {
        type: data.type,
        image: data.src,
        ID: result,
        x: x,
        y: y,
        width: data.width,
        height: data.height,
        trMatrix : [
            [1, 0], // Matrice identité
            [0, 1]
        ],
        rotation: 0,
        miroirGD: false
    };
    composants.push(newObject);
});

cnv.addEventListener('dragover', function(e) {
    e.preventDefault(); // Nécessaire pour permettre le drop
    // prévoir un dragover spécifique pour les points de dérivation qui
    // peuvent être droppés sur un conducteur --> découpage du conducteur
});

// dessin du canvas -----------------------------------------------------------

function drawCanvas() {

    // création du double buffer
    const bufferCanvas = document.createElement('canvas');
    bufferCanvas.width = cnv.width;
    bufferCanvas.height = cnv.height;
    const bufferCtx = bufferCanvas.getContext('2d');
    
    // création du canvas pour tourner et miroirer
    const transformCanvas = document.createElement('canvas');
    const transformCtx = transformCanvas.getContext('2d');

    // Effacer le buffer avant de dessiner les nouvelles images
    bufferCtx.clearRect(0, 0, bufferCanvas.width, bufferCanvas.height);

    // Parcourir chaque objet du tableau composants
    let nImg = 0;
    const totImg = composants.length;
    
    if (totImg == 0) {
        ctxFond.clearRect(0, 0, fond.width, fond.height);
        // quand il ne reste rien parce que l'on a supprimé le dernier composant
    }
    
    composants.forEach(item => {
        const img = new Image();  // Créer un nouvel objet Image
        img.src = item.image;     // Définir la source de l'image

        // Une fois l'image chargée, la dessiner aux coordonnées (x, y) dans le buffer
        img.onload = async function() {            
            const svgUrl = item.image;
            tmpSvgImage = await loadAndModifySVG(svgUrl, '#000000', item.trMatrix);
            bufferCtx.drawImage(tmpSvgImage, item.x, item.y);
                        
            nImg++;            
                if (nImg === totImg) {
                // Effacer le canvas visible
                ctxFond.clearRect(0, 0, fond.width, fond.height);
                // Dessiner le buffer sur le canvas visible
                ctxFond.drawImage(bufferCanvas, 0, 0);
            }
        };
    });
}


// DESSIN du canvas supérieur ( symbole sélectionné )

async function drawSelectCanvas() {
    ctxDraw.clearRect(0, 0, cnv.width, cnv.height);
    if (selectedCompo != -1) {
    //    const svgImage = new Image();       
    //    svgImage.src = composants[selectedCompo].image;

//    loadAndModifySVG(svgImage.src, '#00CC00', function(svgImage) {  //°°°°//

        if (!isDragging) {
            // Charger et modifier l'image SVG (en vert) et les boutons
            const svgBtn1 = new Image();       
            svgBtn1.src = "img/btn_suppr.svg";
            const svgBtn2 = new Image();       
            svgBtn2.src = "img/btn_rotat.svg";
            const svgBtn3 = new Image();       
            svgBtn3.src = "img/btn_mirGD.svg";
            
            editBtn = [
                { name: 'suppr', x: composants[selectedCompo].x - 8,
                                 y: composants[selectedCompo].y + selectSvgImage.height - svgBtn1.height + 8,
                                 width: svgBtn1.width,
                                 height: svgBtn1.height,
                                 active: true },
                { name: 'rotat', x: composants[selectedCompo].x + selectSvgImage.width - svgBtn2.width + 8,
                                 y: composants[selectedCompo].y + selectSvgImage.height - svgBtn2.height + 8,
                                 width: svgBtn2.width,
                                 height: svgBtn2.height,
                                 active: true },
                { name: 'mirGD', x: composants[selectedCompo].x + selectSvgImage.width - svgBtn3.width + 8,
                                 y: composants[selectedCompo].y + selectSvgImage.height - svgBtn3.height - svgBtn2.height + 8,
                                 width: svgBtn3.width,
                                 height: svgBtn3.height,
                                 active: true }
            ];

            
                // Dessiner l'image SVG modifiée au clic
                ctxDraw.drawImage(selectSvgImage, composants[selectedCompo].x, composants[selectedCompo].y); 
                // dessiner les boutons suppression, rotation etc.
                ctxDraw.drawImage(svgBtn1, composants[selectedCompo].x - 8,
                         composants[selectedCompo].y + selectSvgImage.height - svgBtn1.height + 8);
                ctxDraw.drawImage(svgBtn2, composants[selectedCompo].x + selectSvgImage.width - svgBtn2.width + 8,
                         composants[selectedCompo].y + selectSvgImage.height - svgBtn2.height + 8);
                ctxDraw.drawImage(svgBtn3, composants[selectedCompo].x + selectSvgImage.width - svgBtn3.width + 8,
                         composants[selectedCompo].y + selectSvgImage.height - svgBtn3.height - svgBtn2.height + 8);
        } else {
            // pas possible d'utiliser loadAndModifySVG : trop lent affichage tout pourri
            if (selectSvgImage && selectSvgImage.complete) {
                ctxDraw.drawImage(selectSvgImage, composants[selectedCompo].x, composants[selectedCompo].y);
            } else {
                console.log("Image pas encore chargée ou incorrecte", selectSvgImage);
            }
            
            editBtn[0].active = false;
            editBtn[1].active = false;
            editBtn[2].active = false;
        }
    } else {
        ctxDraw.clearRect(0, 0, drawCanvas.width, drawCanvas.height);
        editBtn[0].active = false;
        editBtn[1].active = false;
        editBtn[2].active = false;
    };
}

// Fonction pour redessiner le canvas avec la ligne actuelle
function drawCurrentLine(startX, startY, endX, endY) {
    // Effacer tout le canvas avant de redessiner
    ctxDraw.clearRect(0, 0, cnv.width, cnv.height);

    // Dessiner la ligne entre le point de départ et le point actuel
    ctxDraw.beginPath();
    ctxDraw.moveTo(startX, startY);
    ctxDraw.lineTo(endX, endY);
    ctxDraw.stroke();
}


// deplacement ----------------------------------------------------------------

// fonction de localisation de composant
function checkComposant(mouseX, mouseY) {
    // les coordonnées sont déjà corrigées du décalcahe du canvas
    let indexTrouve = -1;

    // Utilisation de `some` pour pouvoir sortir de la boucle si condition remplie
    composants.some((item, index) => {
        if (mouseX > item.x && mouseX < item.x + item.width &&
            mouseY > item.y && mouseY < item.y + item.height) {
            indexTrouve = index;
            return true;
        }
        return false;
    });
    // Retourner l'index trouvé ou -1 si aucun élément ne correspond
    return indexTrouve;
}


// Fonction pour obtenir les coordonnées de la souris par rapport au canvas
function getMousePos(cnv, event) {
    const rect = cnv.getBoundingClientRect();  // Récupère les coordonnées du canvas
    return {
        x: event.clientX - rect.left,  // Position X par rapport au canvas
        y: event.clientY - rect.top    // Position Y par rapport au canvas
    };
}

// CENTRALISATION DES EVENEMENTS  ++++++++++++++++++++++++++++++++++++++++++++++
let isDragging = false;
let isMouseDown = false;
let clickTimeout;
let dragOffsetX = 0;
let dragOffsetY = 0;
let startDragging = false;

// fonctions pour localiser les clics sur les boutons 
function isInRect(x, y, rect) {
    return (
        x >= rect.x &&
        x <= rect.x + rect.width &&
        y >= rect.y &&
        y <= rect.y + rect.height
    );
}
function checkBtn(x, y) {
    for (let i = 0; i < editBtn.length; i++) {
        const rect = editBtn[i];
        
        // Vérifier uniquement les boutons actifs
        if (rect.active && isInRect(x, y, rect)) {
            return rect.name;  // Renvoie le nom du rectangle trouvé
        }
    }
    return null;  // Si aucune correspondance n'est trouvée
}


cnv.addEventListener('mousedown', async function(e) {
    const startX = e.clientX;
    const startY = e.clientY;
    const MIN_MOVE_THRESHOLD = 3; // Déplacement minimal à partir duquel on considère que c'est un drag

// rechercher, dans l'ordre : clic sur bouton d'édition - mousedown sur borne - mousedown sur le composant
// bouton edition
    const foundeditBtn = checkBtn(startX, startY);
    if (foundeditBtn) {
        if (foundeditBtn == "suppr") {
            console.log('suppression');
            supprComposant(selectedCompo);
            return;
        } 
        if (foundeditBtn == "rotat") {
            console.log('rotation');
            rotatComposant(selectedCompo);
            return;
        } 
        if (foundeditBtn == "mirGD") {
            console.log('miroir');
            mirrorComposant(selectedCompo);
            return;
        } 
        
    } else {
        
        
    }


// composant
    compoEvent = checkComposant(startX, startY);
    if (compoEvent != -1) {
        dragOffsetX = composants[compoEvent].x - e.clientX;
        dragOffsetY = composants[compoEvent].y - e.clientY;
        
        const svgUrl = composants[compoEvent].image;
        selectSvgImage = await loadAndModifySVG(svgUrl, '#00CC00', composants[compoEvent].trMatrix);
    } else {
        dragOffsetX = 0;
        dragOffsetY = 0;
    }
    
    // Commence à écouter les mouvements uniquement après le mousedown
    // ---> evenements imbriqués...
    const onMouseMove = function(e) {
        const currentX = e.clientX;
        const currentY = e.clientY;

        // Vérifier si la souris a suffisamment bougé pour considérer un drag
        if (Math.abs(currentX - startX) > MIN_MOVE_THRESHOLD || Math.abs(currentY - startY) > MIN_MOVE_THRESHOLD) {
            isDragging = true;
            // Action de drag ici (déplacement de l'objet par ex.)
            if (compoEvent != -1) {
                if (!startDragging) {
                    // effectué une fois au début du drag : effacer le composant du canvas persistant
                    startDragging = true;
                    ctxFond.clearRect(composants[compoEvent].x, composants[compoEvent].y, composants[compoEvent].width,  composants[compoEvent].height);
                }
                onDragComposant(currentX, currentY, compoEvent);
            } else {
                // drag mais pas sur un composant
 
            }
        }
    };

    const onMouseUp = function(e) {
        const endX = e.clientX;
        const endY = e.clientY;
        startDragging = false;
        // Si aucun mouvement, c'est un clic, sinon c'est un drag terminé
        if (!isDragging) {
            // clic normal
            if (compoEvent != -1) {
                onComposantClick(compoEvent);
            } else {
                onVideClick();
            }
            
        } else {
            // fin de drag
            isDragging = false;
            if (compoEvent != -1) {
                onEndDragComposant(endX, endY, compoEvent);
            } else {
                // endDrag mais ailleurs que pour un composant
            }
        }

        // On arrête d'écouter le mouvement après le mouseup
        cnv.removeEventListener('mousemove', onMouseMove);
        cnv.removeEventListener('mouseup', onMouseUp);
    };

    cnv.addEventListener('mousemove', onMouseMove);
    cnv.addEventListener('mouseup', onMouseUp);
});

// EVENEMENTS DE MODIFICATION ++++++++++++++++++++++++++++++++++++++++++++++++++

function onComposantClick(id) {
    // prévoir aussi clic sur conducteurs
    //sélectionne si pas déjà selectionné, déselectionne sinon
    selectedCompo = (selectedCompo === id) ? -1 : id;
    drawSelectCanvas();
}

function onVideClick() {
    // prévoir aussi clic sur conducteurs
    selectedCompo = -1;
    drawSelectCanvas();    
}

function onDragComposant(x, y, id) { 
    composants[id].x = x + dragOffsetX;
    composants[id].y = y + dragOffsetY;
    
    selectedCompo = id;
    drawSelectCanvas();  
}

function onEndDragComposant(x, y, id) { 
    drawSelectCanvas();
    drawCanvas();
}


