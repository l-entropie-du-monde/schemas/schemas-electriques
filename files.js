// Fonction pour sauvegarder le tableau dans un fichier JSON
function saveToFile() {
    const json = JSON.stringify(composants, null, 2);  // Sérialisation en JSON
    const blob = new Blob([json], { type: 'application/json' });
    const url = URL.createObjectURL(blob);

    // Créer un lien de téléchargement
    const a = document.createElement('a');
    a.href = url;
    a.download = 'schema.json';  // Nom du fichier à sauvegarder
    a.click();  // Simuler un clic pour déclencher le téléchargement

    // Libérer l'URL après usage
    URL.revokeObjectURL(url);
}

// Fonction pour ouvrir un fichier JSON et restaurer les données dans le tableau
function openFromFile(event) {
    const file = event.target.files[0];  // Récupérer le fichier sélectionné
    if (!file) return;

    const reader = new FileReader();
    reader.onload = function(e) {
        const json = e.target.result;  // Contenu du fichier JSON
        composants = JSON.parse(json);  // Restaurer le tableau
        console.log("Tableau restauré :", composants);
    };
    reader.readAsText(file);  // Lire le fichier en tant que texte
}

// Fonction pour sauvegarder le canvas en tant qu'image PNG
function exportCanvasAsPNG(fileName) {
    // nom du fichier pour l'enregistrement
    const nomFile = "schema.png";
    // récup. de l'élément <canvas>
    const canvas = document.getElementById('fond');
    let dataImage;
    // création d'un lien HTML5 download
    const lien = document.createElement("a");
    // récup. des data de l'image
    dataImage = canvas.toDataURL("image/png");
    // affectation d'un nom à l'image
    lien.download = nomFile;
    // modifie le type de données
    dataImage = dataImage.replace("image/png", "image/octet-stream");
    // affectation de l'adresse
    lien.href = dataImage;
    // ajout de l'élément
    document.body.appendChild(lien);
    // simulation du click
    lien.click();
    // suppression de l'élément devenu inutile
    document.body.removeChild(lien);
}


// Attacher les événements aux boutons

document.getElementById('exportButton').addEventListener('click', function() {
    exportCanvasAsPNG('circuit.png');
});


document.getElementById('saveButton').addEventListener('click', saveToFile);

// Créer un input type=file pour l'ouverture
const inputFile = document.createElement('input');
inputFile.type = 'file';
inputFile.accept = 'application/json';
inputFile.style.display = 'none';  // Masquer l'input

document.body.appendChild(inputFile);  // Ajouter l'input caché au document

// Ouvrir le fichier lors du clic sur le bouton open
document.getElementById('openButton').addEventListener('click', () => {
    inputFile.click();  // Simuler un clic pour ouvrir le sélecteur de fichier
});

inputFile.addEventListener('change', openFromFile);

